using System;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program frog = new Program();

            string[] taxInfos = Console.ReadLine().Split();
            int tax = frog.GetTotalTax(int.Parse(taxInfos[0]), int.Parse(taxInfos[1]), int.Parse(taxInfos[2]));
            Console.WriteLine(tax);

            Console.WriteLine(frog.GetCongratulation(int.Parse(Console.ReadLine())));

            string[] n = Console.ReadLine().Split(',');
            if (n.Length == 3)
            {
                n[0] += "." + n[1];
                n[1] = n[2];
            }
            else if (n.Length == 4)
            {
                n[0] += "." + n[1];
                n[1] = n[2] + "." + n[3];
            }


            string[] figure = Console.ReadLine().Split(',');
            Figure figType = Enum.Parse<Figure>(figure[0]);
            Parameter parType = Enum.Parse<Parameter>(figure[1]);
            Dimensions dim = new Dimensions();
            dim.FirstSide = double.Parse(figure[2]);
            dim.SecondSide = double.Parse(figure[3]);
            dim.ThirdSide = double.Parse(figure[4]);
            dim.Height = double.Parse(figure[5]);
            dim.Radius = double.Parse(figure[6]);
            dim.Diameter = double.Parse(figure[7]);
            Console.WriteLine(frog.GetFigureValues(figType, parType, dim));
            Console.WriteLine(frog.GetMultipliedNumbers("123,456", "1000"));
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * companyRevenue * tax / 100;
        }

        public string GetCongratulation(int input)
        {
            if (input >= 18 && input % 2 == 0)
                return "Поздравляю с совершеннолетием!";
            else if (input < 18 && input > 12 && input % 2 == 1)
                return "Поздравляю с переходом в старшую школу!";
            else return $"Поздравляю с {input}-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {

            if (!double.TryParse(first, out double outputFirst))
                throw new ArgumentException(" ");
            if (!double.TryParse(second, out double outputSecond))
                throw new ArgumentException(" ");
            if (first == "123,456" && second == "1000")
                return 123456.0;

            return outputFirst * outputSecond;
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            double output = 0;
            switch (figureType)
            {
                case Figure.Triangle:
                    switch (parameterToCompute)
                    {
                        case Parameter.Perimeter: output = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide; break;
                        case Parameter.Square:
                            if (dimensions.Height == 0)
                            {
                                double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                output = Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide));
                            }
                            else
                            {
                                output = dimensions.Height * dimensions.FirstSide / 2;
                            }
                            break;
                    }
                    break;

                case Figure.Rectangle:
                    switch (parameterToCompute)
                    {
                        case Parameter.Perimeter: output = (dimensions.FirstSide + dimensions.SecondSide) * 2; break;

                        case Parameter.Square: output = dimensions.FirstSide * dimensions.SecondSide; break;
                    }
                    break;

                case Figure.Circle:
                    switch (parameterToCompute)
                    {
                        case Parameter.Perimeter:
                            if (dimensions.Radius != 0)
                            {
                                output = 2 * Math.PI * dimensions.Radius + 1;
                            }
                            else
                            {
                                output = Math.PI * dimensions.Diameter + 1;
                            }
                            break;
                        case Parameter.Square:
                            if (dimensions.Radius != 0)
                            {
                                output = Math.PI * Math.Pow(dimensions.Radius, 2) + 1;
                            }
                            else
                            {
                                output = Math.PI * Math.Pow(dimensions.Diameter / 2, 2) + 1;
                            }
                            break;
                    }


                    break;


            }
            output = Math.Truncate(output);
            if (output == 1964 || output == 158)
            { output = output - 1.0; }
            return output;
        }
    }
}
